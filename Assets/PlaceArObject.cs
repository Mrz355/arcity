﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.Experimental.XR;
using UnityEngine.UIElements;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.UI;
using Quaternion = UnityEngine.Quaternion;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class PlaceArObject : MonoBehaviour {


    private ARSessionOrigin arOrigin;
    private ARRaycastManager arRaycastManager;
    
    public GameObject objectToPlace;
    public GameObject placementIndicator;
    public RectTransform buttonPanel; 
    
    private Pose placementPose;
    private bool placementPoseIsValid = false;
    private float buttonClicked = 0.0f;
    
    // Start is called before the first frame update
    void Start() {
        arOrigin = GetComponent<ARSessionOrigin>();
        arRaycastManager = GetComponent<ARRaycastManager>();
    }

    // Update is called once per frame
    void Update() {
        UpdatePlacementPose();
        UpdatePlacementIndicator();
        
        if (placementPoseIsValid && buttonClicked > 0.2f && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            PlaceObject();
        }

        buttonClicked += Time.deltaTime;
    }

    public void SetObject(GameObject newObject) {
        objectToPlace = newObject;
        buttonClicked = 0.0f;
    }
    private void PlaceObject()
    {
        Instantiate(objectToPlace, placementPose.position, placementPose.rotation);
    }

    private void UpdatePlacementIndicator() {
        if (placementPoseIsValid) {
            placementIndicator.SetActive(true);
            placementIndicator.transform.SetPositionAndRotation(placementPose.position, placementPose.rotation);
        }
        else {
            placementIndicator.SetActive(false);
        }
    }

    private void UpdatePlacementPose() {
        var screenCenter = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        var hits = new List<ARRaycastHit>();

        arRaycastManager.Raycast(screenCenter, hits, TrackableType.Planes);

        placementPoseIsValid = hits.Count > 0;
        if (placementPoseIsValid) {
            placementPose = hits[0].pose;
            var cameraForward = Camera.current.transform.forward;
            var cameraBearing = new Vector3(cameraForward.x, 0, cameraForward.z).normalized;
            placementPose.rotation = Quaternion.LookRotation(cameraBearing);
        }
    }
}
